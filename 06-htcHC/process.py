import sys

max = -9999999999.0
maxS = ''
for line in sys.stdin:
    if line[0] == '#':
        tokens = line[1:].split()
        v = float(tokens[0])
        if v > max:
            max = v
            maxS = tokens[1:]
print(max, maxS)