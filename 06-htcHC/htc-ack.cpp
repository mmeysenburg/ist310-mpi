#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits>
#include <random>
#include <mpi.h>

long double PI = 3.1415926535897932384626433832795;
long double E = 2.7182818284590452353602874713527;

/**
 * DeJong's 1st function in three dimensions, negated because I like to 
 * maximize instead of minimize: f(x, y) = -(x^2 + y^2). It's a simple function
 * so suggest to the compiler that it could be inlined.
 *
 * \param x 
 * \param y
 * \param f(x, y)
 */
long double ackley(long double x, long double y) {
	long double a = -0.2 * sqrtl(0.5 * (x * x + y * y));
	long double b = 0.5 * (cosl(2 * PI * x) + cosl(2 * PI * y));
	long double c = 20 * expl(a);
	long double d = expl(b);
	return c + d - E - 20;
}

/**
 * Application entry point.
 *
 * \param argc Number of command line arguments; ignored
 * \param argv Command line arguments; ignored
 */
int main(int argc, char** argv) {
	using namespace std;

	int rank;		// processor's rank ID
	int nProcs;		// number of processors in the communicator

	MPI_Init(&argc, &argv);					// set up the communicator
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	// which processor am I?
	MPI_Comm_size(MPI_COMM_WORLD, &nProcs);	// how many processors are there?

	// random number generator and distributions for initial population and
	// steps
	mt19937 prng(time(0) + rank);
	uniform_real_distribution<long double> dist(-5.12, 5.12);
	uniform_real_distribution<long double> dDist(-0.01, 0.01);

	// miscellaneous varaibles
	int n = 10000;		// number of ants in the population
	int steps = 10000;	// number of steps to take
	int i, step;		// loop control variables
	long double x, y;	// temporary variables
	long double v, max;	// ditto 
	long double** ants;	// population of (x, y, f(x, y)) triples

	// allocate memory and set initial values
	ants = new long double* [n];
	for (i = 0; i < n; i++) {
		ants[i] = new long double[3];
		ants[i][0] = dist(prng);
		ants[i][1] = dist(prng);
		ants[i][2] = ackley(ants[i][0], ants[i][1]);
	}

	// perform hill climbing steps
	for (step = 0; step < steps; step++) {
		// each ant takes a step if doing so moves "higher"
		for (i = 0; i < n; i++) {
			x = ants[i][0] + dDist(prng);
			y = ants[i][1] + dDist(prng);
			v = ackley(x, y);

			if (v > ants[i][2]) {
				ants[i][0] = x;
				ants[i][1] = y;
				ants[i][2] = v;
			}
		}
	}

	// after all the steps, search for the maximum value 
	max = -numeric_limits<long double>::max();
	for (i = 0; i < n; i++) {
		if (ants[i][2] > max) {
			x = ants[i][0];
			y = ants[i][1];
			max = ants[i][2];
		}
	}

	// report results
	printf("# %Lf (%Lf, %Lf)\n", max, x, y);

	// free memory
	for (i = 0; i < n; i++) {
		delete[] ants[i];
	}
	delete[] ants; 

	MPI_Finalize();

	return EXIT_SUCCESS;
}