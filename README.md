# ist310-mpi: OpenMPI sample code for IST 310

This repo contains sample code for the OpenMPI module of the spring 2020 IST310
course.

## Contents

### 01-hello

Directory containing the first "Hello from processor x" example.

### 02-countKey

Directory containing uni-processor and OpenMPI code for the key counting example.

### 03-charFreq

Directory containing uni-processor and OpenMPI code for the character frequency
example.

### 04-vectorAdd

Directory containing uni-processor and stub OpenMPI code for the vector 
addition example.

### 05-htcPi

Directory containing uni-processor and OpenMPI code to do Monte Carlo 
estimation of pi.
