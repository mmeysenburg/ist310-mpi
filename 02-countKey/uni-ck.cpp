#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <random>

// array size
int N = 10000000;

/**
 * Uni-processor program to search an array and count the number of times a 
 * given key value appears in the array.
 *
 * usage: bin/uni-ck k
 *
 * where k is the key to search for. Array elements will be random int values 
 * in [0, 10000].
 *
 * \author Mark M. Meysenburg
 * \version 3/23/2020
 */
int main(int argc, char** argv) {
    using namespace std;

    // sanity check on command-line arguments
    if (argc != 2) {
        fprintf(stderr, "usage: bin/uni-ck k");
        exit(EXIT_FAILURE);
    }

    // save command line arguments, create and populate array
    int k = atoi(argv[1]);
    int* array = new int[N];

    mt19937 prng(time(0));
    uniform_int_distribution<int> dist(0, 10000);
    for (int i = 0; i < N; i++) {
        array[i] = dist(prng);
    }

    // perform the search
    int count = 0;
    for (int i = 0; i < N; i++) {
        if (array[i] == k) {
            count++;
        }
    }

    // report results
    printf("Key %d found %d times in array.\n", k, count);

    // free memory and exit the program
    delete[] array;

    return EXIT_SUCCESS;
}