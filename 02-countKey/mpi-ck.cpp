#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <random>
#include <mpi.h>

// array size
int N = 10000000;

/**
 * OpenMPI program to search an array and count the number of times a 
 * given key value appears in the array.
 *
 * usage: mpirun bin/mpi-ck.mpi k
 *
 * where k is the key to search for. Array elements will be random int values 
 * in [0, 10000].
 *
 * \author Mark M. Meysenburg
 * \version 3/23/2020
 */
int main(int argc, char** argv) {
    using namespace std;

    int TAG_K = 1;      // constant tag for sending k
    int TAG_ARRAY = 2;  // constant tag for sending array fragments
    int TAG_COUNT = 3;  // constant tag for sending counts

    int rank;		// processor's rank ID
    int nProcs;		// number of processors in the communicator

    // initialize MPI constructs
    MPI_Init(&argc, &argv);					// set up the communicator
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);	// which processor am I?
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);	// how many processors are there?

    if (rank == 0) {
        // the root process creates the array and distributes it to sub-
        // processors

        // sanity check on command-line arguments
        if (argc != 2) {
            fprintf(stderr, "usage: mpirun bin/mpi-ck.mpi k");
            exit(EXIT_FAILURE);
        }

        // save command line arguments, create and populate array
        int k = atoi(argv[1]);
        int* array = new int[N];

        // check the count when we fill, so we can verify the correct answer
        int realCount = 0;
        mt19937 prng(time(0));
        uniform_int_distribution<int> dist(0, 10000);
        for (int i = 0; i < N; i++) {
            array[i] = dist(prng);
            if (array[i] == k) {
                realCount++;
            }
        }

        // send k to each subprocess
        for (int i = 1; i < nProcs; i++) {
            MPI_Send(&k, 1, MPI_INT, i, TAG_K, MPI_COMM_WORLD);
        }

        // send array fragments to subprocesses
        int start = 0;
        int fragSize = N / (nProcs - 1) + 1;
        for (int i = 1; i < nProcs - 1; i++) {
            MPI_Send(array + start, fragSize, MPI_INT, i, TAG_ARRAY,
                MPI_COMM_WORLD);
            start += fragSize;
        }
        // final fragment may be oddly sized
        MPI_Send(array + start, N - start, MPI_INT, nProcs - 1,
            TAG_ARRAY, MPI_COMM_WORLD);

        // receive and accumulate results from subprocessors
        int count = 0, c;
        MPI_Status status;
        for (int i = 1; i < nProcs; i++) {
            MPI_Recv(&c, 1, MPI_INT, i, TAG_COUNT, MPI_COMM_WORLD, &status);
            count += c;
        }

        // report results
        printf("Found %d %d times (expected %d).\n", k, count, realCount);

        // free memory
        delete[] array;
    }
    else {
        // receive k from master
        int k;
        MPI_Status status;
        MPI_Recv(&k, 1, MPI_INT, 0, TAG_K, MPI_COMM_WORLD, &status);

        // allocate maximum amount of memory we could receive
        int maxN = N / (nProcs - 1) + 1;
        int* array = new int[maxN];
        
        // receive array segment from master
        int n;
        MPI_Recv(array, maxN, MPI_INT, 0, TAG_ARRAY, MPI_COMM_WORLD,
            &status);
        MPI_Get_count(&status, MPI_INT, &n);

        // peform the search
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (array[i] == k) {
                count++;
            }
        }

        // send the count value back to the master process
        MPI_Send(&count, 1, MPI_INT, 0, TAG_COUNT, MPI_COMM_WORLD);

        // free subprocess memory
        delete[] array;
    }

    // shudown communicator and end the program
    MPI_Finalize();

    return EXIT_SUCCESS;
}