#include <cstdlib>
#include <ctime>
#include <iostream>
#include <mpi.h>
#include <random>

int main(int argc, char** argv) {
	using namespace std;

	int rank;		// processor's rank ID
	int nProcs;		// number of processors in the communicator

	MPI_Init(&argc, &argv);					// set up the communicator
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	// which processor am I?
	MPI_Comm_size(MPI_COMM_WORLD, &nProcs);	// how many processors are there?

	mt19937 prng(time(0) + rank);
	uniform_real_distribution<long double> dist(0.0, 1.0);
	int numin = 0;
	long double x, y, d;
	for (int i = 0; i < 100000000; i++) {
		x = dist(prng);
		y = dist(prng);
		d = x * x + y * y;
		if (d <= 1.0) {
			numin++;
		}
	}

	cout << "# " << numin << endl;

	MPI_Finalize();

	return EXIT_SUCCESS;
}