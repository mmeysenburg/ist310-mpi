import sys

sum = 0.0
num = 0.0
for line in sys.stdin:
    if line[0] == '#':
        sum += float(line[1:])
        num += 100000000
print(sum / num * 4.0)