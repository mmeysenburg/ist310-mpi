#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>

int main() {
	using namespace std;
	mt19937 prng(time(0));
	uniform_real_distribution<long double> dist(0.0, 1.0);
	int numin = 0;
	long double x, y, d;
	for (int i = 0; i < 100000000; i++) {
		x = dist(prng);
		y = dist(prng);
		d = x * x + y * y;
		if (d <= 1.0) {
			numin++;
		}
	}

	cout << (numin / 100000000.0 * 4.0) << endl;

	return EXIT_SUCCESS;
}