#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <string>

/**
 * Uni-processor program to count character frequency in a specified UTF-8 text
 * file. 
 *
 * usage: bin/uni-cf <filename>
 *
 * \author Mark M. Meysenburg
 * \version 03/26/2020
 */
int main(int argc, char** argv) {
	using namespace std;

	///////////////////////////////////////////////////////////////////////////
	// sanity check on command line arguments
	///////////////////////////////////////////////////////////////////////////
	if (argc != 2) {
		fprintf(stderr, "usage: bin/uni-cf <filename>");
		return EXIT_FAILURE;
	}

	///////////////////////////////////////////////////////////////////////////
	// read the contents of the text file into an array of characters
	///////////////////////////////////////////////////////////////////////////
	ifstream inFile(argv[1]);

	// this string constructor takes two iterators -- one to the start of the
	// file, and one to the end. The istreambuf_iterator<char> is an iterator
	// designed to do a single pass through a stream like a file, reading 
	// character by character
	string* contents = new string((istreambuf_iterator<char>(inFile)), 
		istreambuf_iterator<char>());

	// now that we have the input file as one huge string, extract the chars
	// into an array
	int n = contents->length() + 1;
	char* array = new char[n];
	std::strcpy(array, contents->c_str());

	// close the file and delete the string; we no longer need that memory
	inFile.close();
	delete contents;

	///////////////////////////////////////////////////////////////////////////
	// count character frequency
	///////////////////////////////////////////////////////////////////////////

	// array holding counts; declaring an array this way initializes all 
	// elements to 0. There's one array element for each possible UTF-8 
	// character code, [0, 255].
	int charCounts[256] = { 0 };

	// examine every character in the array
	for (int i = 0; i < n; i++) {
		// convert char to an index
		int idx = (int)array[i];

		// don't count any non-standard character values in the file
		if (idx >= 0 && idx <= 255) {
			charCounts[idx]++;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	// output counts for printable characters
	///////////////////////////////////////////////////////////////////////////
	for (int i = 32; i <= 126; i++) {
		printf("%03d ('%c'): %d\n", i, (char)i, charCounts[i]);
	}

	///////////////////////////////////////////////////////////////////////////
	// free textfile character arrray and exit the program
	///////////////////////////////////////////////////////////////////////////
	delete[] array;

	return EXIT_SUCCESS;

}