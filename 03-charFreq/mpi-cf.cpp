#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <string>
#include <mpi.h>

/**
 * OpenMPI program to count character frequency in a specified UTF-8 text file.
 *
 * usage: mpirun -n <N> bin/mpi-cf <filename>
 *
 * \author Mark M. Meysenburg
 * \version 03/26/2020
 */
int main(int argc, char** argv) {
	using namespace std;

	///////////////////////////////////////////////////////////////////////////
	// Tags for MPI messages
	///////////////////////////////////////////////////////////////////////////
	int TAG_N = 1;		// for sending subarray size
	int TAG_ARRAY = 2;  // for sending array fragments
	int TAG_COUNTS = 3;	// for returning counts

	///////////////////////////////////////////////////////////////////////////
	// MPI initialization
	///////////////////////////////////////////////////////////////////////////
	int rank;		// processor's rank ID
	int nProcs;		// number of processors in the communicator

	// initialize MPI constructs
	MPI_Init(&argc, &argv);					// set up the communicator
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);	// which processor am I?
	MPI_Comm_size(MPI_COMM_WORLD, &nProcs);	// how many processors are there?

	///////////////////////////////////////////////////////////////////////////
	// Master process portion
	///////////////////////////////////////////////////////////////////////////
	if (rank == 0) {

		///////////////////////////////////////////////////////////////////////
		// sanity check on command line arguments
		///////////////////////////////////////////////////////////////////////
		if (argc != 2) {
			fprintf(stderr, "usage: mpirun -n <N> bin/mpi-cf <filename>");
			return EXIT_FAILURE;
		}

		///////////////////////////////////////////////////////////////////////
		// read the contents of the text file into an array of characters
		///////////////////////////////////////////////////////////////////////
		ifstream inFile(argv[1]);

		// this string constructor takes two iterators -- one to the start of the
		// file, and one to the end. The istreambuf_iterator<char> is an iterator
		// designed to do a single pass through a stream like a file, reading 
		// character by character
		string* contents = new string((istreambuf_iterator<char>(inFile)),
			istreambuf_iterator<char>());

		// now that we have the input file as one huge string, extract the chars
		// into an array
		int n = contents->length() + 1;
		char* array = new char[n];
		std::strcpy(array, contents->c_str());

		// close the file and delete the string; we no longer need that memory
		inFile.close();
		delete contents;

		///////////////////////////////////////////////////////////////////////
		// distribute input array to all other processors
		///////////////////////////////////////////////////////////////////////
		int start = 0;
		int fragSize = n / (nProcs - 1) + 1;
		for (int i = 1; i < nProcs - 1; i++) {
			// send size -- we need to send this because the program doesn't 
			// know how large the input text file is at compile time
			MPI_Send(&fragSize, 1, MPI_INT, i, TAG_N, MPI_COMM_WORLD);
			// send subarray
			MPI_Send(array + start, fragSize, MPI_CHAR, i, TAG_ARRAY,
				MPI_COMM_WORLD);
			start += fragSize;
		}
		// send final fragment
		fragSize = n - start;
		MPI_Send(&fragSize, 1, MPI_INT, nProcs - 1, TAG_N, MPI_COMM_WORLD);
		MPI_Send(array + start, fragSize, MPI_CHAR, nProcs - 1,
			TAG_ARRAY, MPI_COMM_WORLD);

		///////////////////////////////////////////////////////////////////////
		// receive counts from subprocessors
		///////////////////////////////////////////////////////////////////////

		// array for accumulating all the counts		
		int counts[256] = { 0 };
		// array for receiving counts from subprocessors
		int rCounts[256];

		// receive suprocessor counts, accumulate
		MPI_Status status;
		for (int i = 1; i < nProcs; i++) {
			// get counts from each subprocessor
			MPI_Recv(rCounts, 256, MPI_INT, i, TAG_COUNTS, MPI_COMM_WORLD,
				&status);

			// add subprocessor counts to the overall count array
			for (int j = 0; j < 256; j++) {
				counts[j] += rCounts[j];
			}
		}

		///////////////////////////////////////////////////////////////////////
		// output counts for printable characters
		///////////////////////////////////////////////////////////////////////
		for (int i = 32; i <= 126; i++) {
			printf("%03d ('%c'): %d\n", i, (char)i, counts[i]);
		}

		// free memory
		delete[] array;

	}
	else {
		///////////////////////////////////////////////////////////////////////
		// subprocessor portion
		///////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////
		// receive data from root processor 
		///////////////////////////////////////////////////////////////////////

		// receive size of subarray we're expecting
		int n;
		MPI_Status status;
		MPI_Recv(&n, 1, MPI_INT, 0, TAG_N, MPI_COMM_WORLD, &status);

		// allocate space to hold subarray and counts array
		char* array = new char[n];
		int counts[256] = { 0 };

		// recieve subarray to process
		MPI_Recv(array, n, MPI_CHAR, 0, TAG_ARRAY, MPI_COMM_WORLD, &status);

		///////////////////////////////////////////////////////////////////////
		// count characters is this subarray
		///////////////////////////////////////////////////////////////////////
		for (int i = 0; i < n; i++) {
			// convert char to an index
			int d = (int)array[i];

			// don't count any non-standard character values in the file
			if (d >= 0 && d <= 255) {
				counts[d]++;
			}
		}

		///////////////////////////////////////////////////////////////////////
		// send counts back to the root process
		///////////////////////////////////////////////////////////////////////
		MPI_Send(counts, 256, MPI_INT, 0, TAG_COUNTS, MPI_COMM_WORLD);

		// free memory before exiting
		delete[] array;
	}
	
	///////////////////////////////////////////////////////////////////////////
	// shudown communicator and end the program
	///////////////////////////////////////////////////////////////////////////
	MPI_Finalize();

	return EXIT_SUCCESS;

}